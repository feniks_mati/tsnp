﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour
{

    public float speed = 10f;
    private const int outOfRange = 18;

    void Start()
    {
        gameObject.name = "Ball";
    }

    void FixedUpdate()
    {
        if (transform.position.x > outOfRange)
        {
            Destroy(gameObject);
        }
        transform.Translate(Vector3.right * Time.fixedDeltaTime * speed);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(GameManager.tagEnemy))
        {
            TryToKillEnemy(other.gameObject);
        }
    }

    void TryToKillEnemy(GameObject enemy)
    {
        Mover mover = enemy.GetComponent<Mover>();
        if (!mover.Death)
        {
            mover.Die();
            mover.StartDelete();
        }
    }
}
