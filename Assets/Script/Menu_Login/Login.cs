﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Login : MonoBehaviour {

    public static string userName = null;
    public static string password = null;
    public static string confirmPassword = null;

    private string createAccountURL = "http://gestwa.smarthost.pl/tsnp/createUserAccount.php";
    private string createLoginURL = "http://gestwa.smarthost.pl/tsnp/loginUser.php";

    public Button createAccountButton;
    public Button loginButton;
    public Button createAccountConfButton;
    public Button createAccountBackButton;
    public Button createAccountErrorOkButton;
    public Button createAccountConnectionErrorButton;
    public Button loginConnectionErrorOkButton;
    public Button loginErrorOkButton;
    public Button offlineOkButton;

    public InputField userNameInputField;
    public InputField passwordInputField;
    public InputField accountUserNameInputField;
    public InputField accountPasswordInputField;
    public InputField accountPasswordConfirmInputField;

    public GameObject loginPanel;
    public GameObject createAccountPanel;
    public GameObject createAccountResultPanel;
    public GameObject connectionAccountErrorPanel;
    public GameObject connectionLoginErrorPanel;
    public GameObject loginErrorPanel;
    public GameObject offlineNotificationPanel;

    public Text loginErrorText;
    public Text accountErrorText;

    // Use this for initialization
    void Start () {

        if (PlayerPrefs.HasKey("Name") && PlayerPrefs.HasKey("Pass"))
        {
            userName = PlayerPrefs.GetString("Name");
            password = PlayerPrefs.GetString("Pass");
            StartCoroutine("LoginIntoAccount");
        } else {
            loginPanel.gameObject.SetActive(true);
        }

    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void onLoginClick()
    {
        StartCoroutine("LoginIntoAccount");
    }
    public void onCreateAccountClick()
    {
        loginPanel.SetActive(false);
        createAccountPanel.gameObject.SetActive(true);
    }
    public void onCreateAccountConfClick()
    {
        StartCoroutine("CreateAccount");
    }
    public void onCreateAccountBackClick()
    {
        createAccountPanel.gameObject.SetActive(false);
        loginPanel.SetActive(true);
    }
    public void oncreateAccountErrorOkClick()
    {
        createAccountResultPanel.gameObject.SetActive(false);
        if(accountErrorText.text=="Success")
            loginPanel.gameObject.SetActive(true);
        else
            createAccountPanel.gameObject.SetActive(true);
    }
    public void onCreateAccountConnectionErrorOkClick()
    {
        connectionAccountErrorPanel.gameObject.SetActive(false);
        createAccountPanel.gameObject.SetActive(true);
    }
    public void onLoginErrorOkClick()
    {
        loginErrorPanel.gameObject.SetActive(false);
        loginPanel.gameObject.SetActive(true);
    }
    public void onloginConnectionErrorOkClick()
    {
        connectionLoginErrorPanel.gameObject.SetActive(false);
        loginPanel.gameObject.SetActive(true);
    }
    public void onOfflinePlayClick()
    {
        loginPanel.gameObject.SetActive(false);
        offlineNotificationPanel.gameObject.SetActive(true);
    }
    public void onOfflineNotificationOkClick()
    {
        SceneManager.LoadScene("menu");
    }

    IEnumerator CreateAccount(){

        userName = accountUserNameInputField.text;
        password = accountPasswordInputField.text;
        confirmPassword = accountPasswordConfirmInputField.text;

        WWWForm wwwform = new WWWForm();
        wwwform.AddField("userNamePost", userName);
        wwwform.AddField("userPasswordPost", password);
        wwwform.AddField("userConfirmPasswordPost", confirmPassword);

        WWW www = new WWW(createAccountURL, wwwform);
        yield return www;
        if (www.error != null) {
            connectionAccountErrorPanel.gameObject.SetActive(true);
            createAccountPanel.gameObject.SetActive(false);
        } else {
            string createAccountReturn = www.text;
            accountErrorText.text = createAccountReturn;
            createAccountPanel.gameObject.SetActive(false);
            createAccountResultPanel.gameObject.SetActive(true);
        }
    }

    public IEnumerator LoginIntoAccount() {
        WWWForm wwwform = new WWWForm();
        if (!PlayerPrefs.HasKey("Name")) {
            userName = userNameInputField.text;
            password = passwordInputField.text;
        } else {
            userName = PlayerPrefs.GetString("Name");
            password = PlayerPrefs.GetString("Pass");
        }
        wwwform.AddField("userNamePost", userName);
        wwwform.AddField("userPasswordPost", password);

        WWW www = new WWW(createLoginURL, wwwform);
        yield return www;
        if (www.error != null) {
            if (PlayerPrefs.HasKey("validAccount")){
                loginPanel.gameObject.SetActive(false);
                SceneManager.LoadScene("menu");
            } else {
                loginPanel.gameObject.SetActive(false);
                connectionLoginErrorPanel.gameObject.SetActive(true);
            }
        }
        else {
            string loginIntoAccountReturn = www.text;
            if (loginIntoAccountReturn == "Success") {
                PlayerPrefs.SetString("Name", userName);
                PlayerPrefs.SetString("Pass", password);
                PlayerPrefs.SetString("validAccount", "true");
                Debug.Log("Login successfull");
                SceneManager.LoadScene("menu");
                userName = "";
                password = "";
            } else {
                loginErrorText.text = loginIntoAccountReturn;
                loginErrorPanel.gameObject.SetActive(true);
                loginPanel.gameObject.SetActive(false);
            }
        }
    }
}
