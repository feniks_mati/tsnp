﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoutPlay : MonoBehaviour {

    public void OnMouseDown1()
    {
        PlayerPrefs.DeleteKey("Name");
        PlayerPrefs.DeleteKey("Pass");
        PlayerPrefs.DeleteKey("validAccount");
        SceneManager.LoadScene("login");
    }
}
