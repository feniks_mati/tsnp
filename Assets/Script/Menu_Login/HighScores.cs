﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScores : MonoBehaviour {

    public string[] items;
    public string[] namesArray;
    public string[] scoresArray;
    private string getTop10ScoreURL = "http://gestwa.smarthost.pl/tsnp/getTop10Score.php";
    private string getUserHighestScoreURL = "http://gestwa.smarthost.pl/tsnp/getUserHighestScore.php";
    private string userName;
    private string userHighestScore;
    private int arraySize;

    private bool scoreBoardDisplayed = false;

    public GameObject highScorePanel;

    public Text userNameText;
    public Text userScoreText;

    public Text User1NameText;
    public Text User1ScoreText;
    public Text User2NameText;
    public Text User2ScoreText;
    public Text User3NameText;
    public Text User3ScoreText;
    public Text User4NameText;
    public Text User4ScoreText;
    public Text User5NameText;
    public Text User5ScoreText;
    public Text User6NameText;
    public Text User6ScoreText;
    public Text User7NameText;
    public Text User7ScoreText;
    public Text User8NameText;
    public Text User8ScoreText;
    public Text User9NameText;
    public Text User9ScoreText;
    public Text User10NameText;
    public Text User10ScoreText;

    void Start () {
        if (PlayerPrefs.HasKey("Name"))
        {
            userName = PlayerPrefs.GetString("Name");
        }
        else
            userName = "annonymous";
    }
	
	// Update is called once per frame
	void Update () {
	    if(highScorePanel.activeSelf==true && scoreBoardDisplayed==false)
        {
            OnHighScoresClick();
            scoreBoardDisplayed = true;
        }
	}

    public void OnHighScoresClick()
    {
        StartCoroutine("GetTop10Scores");
        StartCoroutine(GetUserHighestScore(userName));
        highScorePanel.gameObject.SetActive(true);
    }
    public void onExitClick()
    {
        Debug.Log(highScorePanel.gameObject.activeInHierarchy);
        highScorePanel.gameObject.SetActive(false);
        Debug.Log("DELETE TAB");
        Debug.Log(highScorePanel.gameObject.activeInHierarchy);
    }

    public IEnumerator GetTop10Scores()
    {
        WWW top10Score = new WWW(getTop10ScoreURL);
        yield return top10Score;
        if (top10Score.error != null)
        {
            if (PlayerPrefs.HasKey("Top10Size")) {
                int size = PlayerPrefs.GetInt("Top10Size");
                namesArray = new string[size - 1];
                scoresArray = new string[size - 1];
                for (int i = 0; i < size-1; i++)
                {
                    if (PlayerPrefs.HasKey("Top10Name" + i) && PlayerPrefs.HasKey("Top10Score" + i))
                    {
                        namesArray[i] = PlayerPrefs.GetString("Top10Name" + i);
                        scoresArray[i] = PlayerPrefs.GetString("Top10Score" + i);
                    }
                }
            }
        } else {
            string top10ScoreString = top10Score.text;
            items = top10ScoreString.Split(';');
            PlayerPrefs.SetInt("Top10Size", items.Length);
            namesArray = new string[items.Length - 1];
            scoresArray = new string[items.Length - 1];
            for (int i = 0; i < items.Length - 1; i++)
            {
                namesArray[i] = GetDataValue(items[i], "Name:");
                scoresArray[i] = GetDataValue(items[i], "Score:");
                PlayerPrefs.SetString("Top10Name" + i, namesArray[i]);
                PlayerPrefs.SetString("Top10Score" + i, scoresArray[i]);
            }
        }
        DisplayTop10();
    }

    IEnumerator GetUserHighestScore(string userName)
    {
        WWWForm wwwform = new WWWForm();
        wwwform.AddField("userNamePost", userName);

        WWW scoreData = new WWW(getUserHighestScoreURL, wwwform);
        yield return scoreData;
        items = scoreData.text.Split(';');
        if (scoreData.error != null || scoreData.text=="") {
            Debug.Log("Return www error "+scoreData.error+" content "+scoreData.text);
            userHighestScore = PlayerPrefs.GetString(userName + "HighestScore", "0");
        }
        else
        {
            Debug.Log("Return www response "+scoreData.text);
            if (userName=="annonymous")
                userHighestScore = PlayerPrefs.GetString("annonymousScore", "0");
            else
            {
                userHighestScore = items[0];
                PlayerPrefs.SetString(userName + "HighestScore", userHighestScore);
            }
        }
        DisplayUserHighestScore();
    }

    string GetDataValue(string data, string index) {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|"))
            return value.Remove(value.IndexOf('|'));
        else
            return value;

    }

    void DisplayTop10()
    {
        if (namesArray.Length >= 1)
        {
            User1NameText.text = namesArray[0];
            User1ScoreText.text = scoresArray[0];
        }
        if (namesArray.Length >= 2)
        {
            User2NameText.text = namesArray[1];
            User2ScoreText.text = scoresArray[1];
        }
        if (namesArray.Length >= 3)
        {
            User3NameText.text = namesArray[2];
            User3ScoreText.text = scoresArray[2];
        }
        if (namesArray.Length >= 4)
        {
            User4NameText.text = namesArray[3];
            User4ScoreText.text = scoresArray[3];
        }
        if (namesArray.Length >= 5)
        {
            User5NameText.text = namesArray[4];
            User5ScoreText.text = scoresArray[4];
        }
        if (namesArray.Length >= 6)
        {
            User6NameText.text = namesArray[5];
            User6ScoreText.text = scoresArray[5];
        }
        if (namesArray.Length >= 7)
        {
            User7NameText.text = namesArray[6];
            User7ScoreText.text = scoresArray[6];
        }
        if (namesArray.Length >= 8)
        {
            User8NameText.text = namesArray[7];
            User8ScoreText.text = scoresArray[7];
        }
        if (namesArray.Length >= 9)
        {
            User9NameText.text = namesArray[8];
            User9ScoreText.text = scoresArray[8];
        }
        if (namesArray.Length >= 10)
        {
            User10NameText.text = namesArray[9];
            User10ScoreText.text = scoresArray[9];
        }
    }
    void DisplayUserHighestScore()
    {
        userNameText.text = userName;
        userScoreText.text = userHighestScore;
    }
}
