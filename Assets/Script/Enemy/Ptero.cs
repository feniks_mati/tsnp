﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ptero : Mover {

    [Header("Ptero")]
    public float maxY;
    public float minY;

    private bool goUp = true;
    private bool goDown = false;


    protected override void Move()
    {
        if (!Death && !freeze)
        {
            transform.position += -transform.right * speed * Time.fixedDeltaTime;
            if (transform.position.y >= maxY)
            {
                goUp = false;
                goDown = true;
            }

            if (transform.position.y <= minY)
            {
                goUp = true;
                goDown = false;
            }

            if (goUp)
            {
                transform.position += transform.up * speed * Time.fixedDeltaTime;
            }
            if (goDown)
            {
                transform.position += -transform.up * speed * Time.fixedDeltaTime;
            }
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collision) { }
    
    
}
