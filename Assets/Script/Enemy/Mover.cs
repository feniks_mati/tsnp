﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
    [Header("Mover")]
    public float speed;
    public float score = 0.0f;
    public float hp = 100.0f;
    public float Hp
    {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
            if (hp <= 0 && !Death)
            {
                Die();
                StartDelete();
            }
        }
    }
    public float damage = 10;
    public float rateAttack = 3f;
    public AudioSource audio;

    protected bool freeze = false;
    protected bool stopMove = false;

    private Collider2D collider2d;
    private Obstacles obstacle;
    private Animator animator;

    private bool unlockedInput = true;
    private float timer = 0.0f;
    private float freezeTimer = 0.0f;
    private float timerAttack = 0.0f;

    private const float takenDamage = 10f;
    private const float freezeDuration = 3.0f;
    private const float lockedInputDuration = 0.2f;


    public delegate void GameManagerEvent(float score);
    public static event GameManagerEvent OnSendScore;

    public delegate void SpawnEvent(GameObject dino);
    public static event SpawnEvent OnDeleteDino;

    private bool death = false;

    public bool Death
    {
        get
        {
            return death;
        }

        set
        {
            death = value;
        }
    }

    void Start()
    {
        audio = GetComponent<AudioSource>();
        collider2d = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        timerAttack = rateAttack + 1;
    }

    void FixedUpdate()
    {
        if (obstacle != null)
        {
            Attack();
        }

        Move();
    }

    void Update()
    {
        CheckObstacles();
        CheckFrozenSpell();
        if (unlockedInput && !Death)
        {
            CheckInput();
        }

        timer += Time.deltaTime;
        if (timer >= lockedInputDuration)
        {
            unlockedInput = true;
        }
    }

    private void CheckObstacles()
    {
        if (obstacle)
        {
            if (obstacle.Health <= 0)
            {
                obstacle = null;
                stopMove = false;
            }
        }
    }

    private void CheckFrozenSpell()
    {
        if (freeze)
        {
            freezeTimer += Time.deltaTime;
            if (freezeTimer >= freezeDuration)
            {
                freeze = false;
                animator.SetBool("frozen", false);
                freezeTimer = 0.0f;
            }
        }
    }

    virtual protected void Move()
    {
        if (!Death && !freeze && !stopMove)
        {
            transform.position += -transform.right * speed * Time.fixedDeltaTime;
        }
    }

    void Attack()
    {
        if (timerAttack >= rateAttack)
        {
            obstacle.Health -= damage;
            timerAttack = 0f;
        }
        timerAttack += Time.fixedDeltaTime;
    }

    void CheckInput()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        if ( Input.GetTouch(0).phase==TouchPhase.Ended)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
#else
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
#endif
            Vector2 touchPos = new Vector2(wp.x, wp.y);
            if (collider2d == Physics2D.OverlapPoint(touchPos))
            {
                TakeDamage();
                unlockedInput = false;
                timer = 0.0f;
            }
        }
    }

    private void TakeDamage()
    {
        Hp -= takenDamage;
    }

    public void StartFreeze()
    {
        freeze = true;
        animator.SetBool("frozen", true);
    }

    public void StartDelete()
    {
        if (OnDeleteDino != null)
        {
            OnDeleteDino(gameObject);
        }
        GetComponent<DestroyByTime>().enabled = true;
    }

    public void Die()
    {
        audio.Play();
        animator.SetBool("death", true);

        if (OnSendScore != null)
            OnSendScore(score);

        Death = true;
    }

    virtual protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacles"))
        {
            obstacle = collision.GetComponent<Obstacles>() as Obstacles;
            stopMove = true;
        }
    }


}
