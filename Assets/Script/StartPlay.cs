﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartPlay : MonoBehaviour {

    public GameObject loading;
    public GameObject head;

    private AsyncOperation asyn;
    private const int startPlayScene = 1;
    
	public void OnMouseDownClick()
    {    
        asyn=SceneManager.LoadSceneAsync(startPlayScene);
    }

    void Update()
    {
        if(asyn!=null)
        {
            loading.SetActive(true);
            head.SetActive(true);
        }
    }
}
