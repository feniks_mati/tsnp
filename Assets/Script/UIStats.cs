﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStats : MonoBehaviour {

    public Text lifeText;
    public Text scoreText;
    public Text gameOverText;
    public Text waveText;
    public Text winText;

    public void DrawLifeText(int life)
    {
        lifeText.text = "" + life;
    }

    public void DrawScoreText(float score)
    {
        scoreText.text = "" + score;
    }

    public void DrawGameOverText()
    {
        gameOverText.text = "GAME OVER";
    }

    public void DrawWaveText(int wave)
    {
        waveText.text = "" + wave;
    }

    public void DrawWinText()
    {
        if (winText.text == "")
        {
            winText.text = "YOU WIN";
        }
    }

}
