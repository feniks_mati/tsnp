﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    public int life = 20;
    public int Life
    {
        get
        {
            return life;
        }
        set
        {
            life = value;

            if (Life <= 0)
            {
                life = 0;
                if (OnEndGame != null)
                {
                    OnEndGame();
                }
                uiStats.DrawGameOverText();
            }
            uiStats.DrawLifeText(life);
        }
    }
    private float mainScore = 0;

    public float MainScore
    {
        get
        {
            return mainScore;
        }
        set
        {
            mainScore = value;
            uiStats.DrawScoreText(mainScore);
        }
    }

    private string userName;
    private string sendUserScoreURL = "http://gestwa.smarthost.pl/tsnp/sendUserScore.php";
    private bool scoreSent = false;

    public GameObject scorePanel;
    public SpawnEnemy spawnEnemy;
    public UIStats uiStats;

    public static event Action OnEndGame;
    public static string tagEnemy = "Enemy";
    private const string annonymousPlayer = "annonymous";
    private const string prefsKeyName = "Name";
    private const string prefsKeyAnnonymousScore = "annonymousScore";
    private const string sceneMenu = "menu";


    void Start()
    {
        SetUserName();
        uiStats.DrawScoreText(mainScore);
        uiStats.DrawLifeText(life);
    }

    private void OnEnable()
    {
        EnemyDetector.OnDetectEnemy += TakeDamage;
    }

    private void OnDisable()
    {
        EnemyDetector.OnDetectEnemy -= TakeDamage;
    }

    void SetUserName()
    {
        if (PlayerPrefs.HasKey(prefsKeyName))
        {
            userName = PlayerPrefs.GetString(prefsKeyName);
        }
        else
        {
            userName = annonymousPlayer;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(tagEnemy))
        {
            TakeDamage();
        }

        other.GetComponent<Mover>().StartDelete();
    }

    void TakeDamage()
    {
        Life--;
    }

    void Update()
    {
        if (Life <= 0 && scoreSent == false)
        {
            TryToSendScore();
        }
        CheckInputEscape();
    }

    void TryToSendScore()
    {
        if (userName == annonymousPlayer)
        {
            PlayerPrefs.SetString(prefsKeyAnnonymousScore, MainScore.ToString());
        }
        else
        {
            StartCoroutine(SendUserScore(userName, mainScore.ToString()));
            scoreSent = true;
        }
        scoreSent = true;
        //scorePanel.gameObject.SetActive(true);
    }

    void CheckInputEscape()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(sceneMenu);
    }

    public void AddScore(float a)
    {
        if (Life > 0)
            mainScore += a;
    }

    IEnumerator SendUserScore(string userName, string userScore)
    {
        WWWForm wwwform = new WWWForm();
        wwwform.AddField("userNamePost", userName);
        wwwform.AddField("userScorePost", userScore);

        WWW www = new WWW(sendUserScoreURL, wwwform);
        yield return www;
        if (PlayerPrefs.HasKey(prefsKeyName))
        {
            if (www.error != null)
            {
                int i = PlayerPrefs.GetInt("offlineScoreSize", 0);
                PlayerPrefs.SetInt(userName + i, int.Parse(userScore));
                PlayerPrefs.SetInt("offlineScoreSize", i + 1);
                Debug.Log("score offline" + i);
                string userHighestScore = (PlayerPrefs.GetString(userName + "HighestScore", "0"));
                if (int.Parse(userScore) > int.Parse(userHighestScore))
                    PlayerPrefs.SetString(userName + "HighestScore", userHighestScore);
            }
            else
            {
                Debug.Log("score online");
                int scoreOfflineSize = PlayerPrefs.GetInt("offlineScoreSize", 0);
                for (int i = 0; i < scoreOfflineSize; i++)
                {
                    if (PlayerPrefs.HasKey(userName + i))
                    {
                        int score = PlayerPrefs.GetInt(userName + i);
                        WWWForm wwwform1 = new WWWForm();
                        wwwform1.AddField("userNamePost", userName);
                        wwwform1.AddField("userScorePost", score);
                        WWW www1 = new WWW(sendUserScoreURL, wwwform1);
                        yield return www1;
                        PlayerPrefs.DeleteKey(userName + i);
                        Debug.Log("score " + i + " deleted");
                    }
                }
                PlayerPrefs.SetInt("offlineScoreSize", 0);
            }
        }
    }

}
