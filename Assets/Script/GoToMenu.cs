﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoToMenu : MonoBehaviour {
   

    public GameObject loading;
    public GameObject head;

    private const int goToMenuScene = 0;

    private AsyncOperation asyn;
    public void ComeBack()
    {
        asyn = SceneManager.LoadSceneAsync(goToMenuScene);
    }

    void Update()
    {
        if (asyn != null)
        {
            loading.SetActive(true);
            head.SetActive(true);
        }
    }


}
