﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyDetector : MonoBehaviour {

    public static event Action OnDetectEnemy;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(GameManager.tagEnemy) && OnDetectEnemy != null)
        {
            OnDetectEnemy();
        }
        other.GetComponent<Mover>().StartDelete();
    }
}
