﻿using UnityEngine;
using System.Collections;

public class Disappearance : MonoBehaviour
{
    private Material material;
    public float fade = 2.5f;

    void Start()
    {
        material = GetComponent<Renderer>().material;
    }

    void FixedUpdate()
    {
        Disappear();
    }

    void Disappear()
    {
        Color color = material.color;
        material.color = new Color(color.r, color.g, color.b, color.a - (fade * Time.fixedDeltaTime));
        if (material.color.a == 0)
        {
            Destroy(gameObject);
        }
    }
}
