﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


[System.Serializable]
public class Wave
{
    public GameObject[] enemyPerfab;
    public float[] spawnInterval;
    public int[] maxEnemies;
}

public class SpawnEnemy : MonoBehaviour
{

    public Wave[] waves;
    public float timeBetweenWaves = 5.0f;
    public Vector3 spawnValues;
    public UIStats uiStats;

    private List<GameObject> listEnemies = new List<GameObject>();
    private int indexWaves;
    private bool startSpawn = true;
    private int sumEnemies;
    private float[] timer;
    private bool canSpawn = true;
    private AudioSource audioFrozen;

    private const int maxPrefabEnemies = 4;

    private float cachedTimeBetweenWaves;

    void Start()
    {
        Initialize();
    }

    private void OnEnable()
    {
        GameManager.OnEndGame += StopSpawn;
    }

    private void OnDisable()
    {
        GameManager.OnEndGame -= StopSpawn;
    }

    private void Initialize()
    {
        cachedTimeBetweenWaves = timeBetweenWaves;
        timer = new float[maxPrefabEnemies];
        indexWaves = 0;
        sumEnemies = 0;
        audioFrozen = GetComponent<AudioSource>();
    }

    void Update()
    {

        UpdateSpawnState();
    }

    private void UpdateSpawnState()
    {
        if (indexWaves < waves.Length && canSpawn)
        {
            CalculateAmountEnemies();

            if (startSpawn && sumEnemies > 0)
            {
                TryToSpawnEnemies();
            }

            for (int i = 0; i < timer.Length; i++)
            {
                timer[i] += Time.deltaTime;
            }

            if (sumEnemies == 0)
            {
                CheckTimeBetweenWaves();
            }
        }
        else
        {
            uiStats.DrawWinText();
        }
        uiStats.DrawWaveText(indexWaves);
    }

    void CalculateAmountEnemies()
    {
        sumEnemies = 0;
        for (int i = 0; i < waves[indexWaves].maxEnemies.Length; i++)
        {
            sumEnemies += waves[indexWaves].maxEnemies[i];
        }
    }

    void TryToSpawnEnemies()
    {
        for (int i = 0; i < waves[indexWaves].enemyPerfab.Length; i++)
        {
            if (waves[indexWaves].maxEnemies[i] > 0 && timer[i] > waves[indexWaves].spawnInterval[i])
            {
                Spawn(waves[indexWaves].enemyPerfab[i]);
                waves[indexWaves].maxEnemies[i]--;
                timer[i] = 0.0f;
            }
        }
    }

    void CheckTimeBetweenWaves()
    {
        startSpawn = false;
        timeBetweenWaves -= Time.deltaTime;
        if (timeBetweenWaves < 0)
        {
            indexWaves++;
            timeBetweenWaves = cachedTimeBetweenWaves;
            startSpawn = true;
        }
    }


    void Spawn(GameObject hazard)
    {
        Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
        Quaternion spawnRotation = Quaternion.identity;
        Instantiate(hazard, spawnPosition, spawnRotation);
    }

    public void StopSpawn()
    {
        canSpawn = false;
    }

    public void DeleteFromList(GameObject g)
    {
        listEnemies.Remove(g);
    }

    public void Freeze()
    {
        audioFrozen.Play();
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            g.GetComponent<Mover>().StartFreeze();
        }
    }


}
