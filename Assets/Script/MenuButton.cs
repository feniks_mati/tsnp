﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {

    private const int startGameScene = 0;

    public void ButtonStart()
    {
        Application.LoadLevel(startGameScene);
    }

    public void ButtonExit()
    {
        Application.Quit();
    }

}
