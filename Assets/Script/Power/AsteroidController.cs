﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AsteroidController : MonoBehaviour
{

    public GameObject asteroidPrefab;
    public GameObject hole;
    public float speed = 5;
    private GameObject movingObject;
    private GameObject target;
    private float timer = 0.0f;
    bool choosenTargetPlace = false;
    bool asteroidStartMoving = false;
    Vector3 touchPos;
    AudioSource audioAsteroid;

    private const float offsetDelay = 0.5f;
    private Vector3 asteroidPosition = new Vector3(-11, 15, -2);

    void Start()
    {
        audioAsteroid = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (choosenTargetPlace)
        {
            CheckInputTarget();
        }

        if (asteroidStartMoving)
        {
            MoveAsteroid();
        }
    }

    private void CheckInputTarget()
    {
        timer += Time.deltaTime;
        if (Input.touchCount == 1 && timer >= offsetDelay)
        {
            CreateAsteroid();
        }
    }

    private void CreateAsteroid()
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        touchPos = new Vector3(wp.x, wp.y, -2);
        movingObject = Instantiate(asteroidPrefab, asteroidPosition, Quaternion.identity) as GameObject;
        asteroidStartMoving = !asteroidStartMoving;
        choosenTargetPlace = !choosenTargetPlace;
    }

    private void MoveAsteroid()
    {
        movingObject.transform.position = Vector3.MoveTowards(movingObject.transform.position, touchPos, Time.deltaTime * speed);
        CheckIfGetTarget();     
    }

    private void CheckIfGetTarget()
    {
        if (movingObject.transform.position == touchPos)
        {
            MakeExplosion();
            FindEnemy();
        }
    }

    private void MakeExplosion()
    {
        GameObject.Instantiate(hole, movingObject.transform.position, Quaternion.identity);
        audioAsteroid.Play();
        asteroidStartMoving = !asteroidStartMoving;

        Destroy(movingObject);
        GetComponent<Image>().color = Color.white;
    }

    private void FindEnemy()
    {
        target = new GameObject("Target");
        target.transform.position = touchPos;
        target.AddComponent<CircleCollider2D>();
        target.GetComponent<CircleCollider2D>().radius = 3.16f;

        foreach (GameObject dinosaur in GameObject.FindGameObjectsWithTag(GameManager.tagEnemy))
        {
            if (target.GetComponent<CircleCollider2D>().OverlapPoint(new Vector2(dinosaur.transform.position.x, dinosaur.transform.position.y)))
            {
                Mover mover = dinosaur.GetComponent<Mover>();
                if (mover.Death)
                {
                    continue;
                }
                mover.Die();
                mover.StartDelete();
            }
        }

        Destroy(target);
    }

    public void ChoosePlace()
    {
        timer = 0.0f;
        choosenTargetPlace = !choosenTargetPlace;
        GetComponent<Image>().color = Color.green;
    }


}
