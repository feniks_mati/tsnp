﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Carnige : MonoBehaviour
{

    public GameObject cannon;
    public GameObject cannonball;
    public Transform[] transformTargetPoints;

    private GameObject[] cannonTab;
    private Vector3[] targetPlace;
    private float speed;
    private bool shoot = false;
    private bool startShooting = false;
    private bool stopShooting = false;
    private float timer = 0.0f;
    private int countShooting = 0;
    private AudioSource audio;

    private const float offsetPosition = 1.61f;
    private const float offsetShooting = 4f;
    private const float rateAttack = 3.0f;
    private const int maxCountShooting = 5;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        CreateCannons();
    }

    void CreateCannons()
    {
        cannonTab = new GameObject[transformTargetPoints.Length];
        targetPlace = new Vector3[transformTargetPoints.Length];

        for (int i = 0; i < transformTargetPoints.Length; i++)
        {
            cannonTab[i] = Instantiate(cannon, transformTargetPoints[i].position, Quaternion.identity) as GameObject;
            targetPlace[i] = new Vector3(transformTargetPoints[i].position.x + offsetPosition, transformTargetPoints[i].position.y, transformTargetPoints[i].position.z);
        }
    }

    void FixedUpdate()
    {
        if (startShooting)
        {
            MoveForwardCannons();
        }

        CheckIfCanShoot();

        if (stopShooting)
        {
            MoveBackCannons();
        }

        timer += Time.fixedDeltaTime;
    }

    private void CheckIfCanShoot()
    {
        if (shoot && timer >= rateAttack)
        {
            countShooting++;
            Shoot();
            timer = 0.0f;
        }
    }

    private void MoveForwardCannons()
    {
        for (int i = 0; i < transformTargetPoints.Length; i++)
        {
            cannonTab[i].transform.position = Vector3.MoveTowards(cannonTab[i].transform.position, targetPlace[i], Time.fixedDeltaTime);
        }

        if (cannonTab[0].transform.position == targetPlace[0])
        {
            startShooting = false;
            shoot = true;
            timer = 0.0f;
            countShooting = 0;
        }
    }

    void Shoot()
    {
        if (countShooting % 2 == 1)
        {
            audio.Play();
            GameObject.Instantiate(cannonball, new Vector3(targetPlace[0].x - offsetShooting, targetPlace[0].y, targetPlace[0].z), Quaternion.identity);
            GameObject.Instantiate(cannonball, new Vector3(targetPlace[2].x - offsetShooting, targetPlace[2].y, targetPlace[2].z), Quaternion.identity);
        }

        else if (countShooting % 2 == 0)
        {
            audio.Play();
            GameObject.Instantiate(cannonball, new Vector3(targetPlace[1].x - offsetShooting, targetPlace[1].y, targetPlace[1].z), Quaternion.identity);
        }

        if (countShooting == maxCountShooting)
        {
            shoot = false;
            stopShooting = true;
        }
    }

    private void MoveBackCannons()
    {
        for (int i = 0; i < transformTargetPoints.Length; i++)
        {
            cannonTab[i].transform.position = Vector3.MoveTowards(cannonTab[i].transform.position, transformTargetPoints[i].position, Time.fixedDeltaTime);
        }

        if (cannonTab[0].transform.position == transformTargetPoints[0].position)
        {
            stopShooting = false;
        }
    }

    public void DanseMasacre()
    {
        startShooting = true;
    }
}
