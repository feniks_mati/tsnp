﻿using UnityEngine;

public class TouchCamera : MonoBehaviour {


    public float speedSwipeCamera = 0.01f;
    private const float speedSwipeCameraMouse= 1f;
    [Header("Boundary Map X")]
    public float leftLimit = 0;
    public float rightLimit=0;
    private Vector2 touchPressPosition;
    private Vector3 swipePosition;

    private Vector3 mouseOrigin;
    private bool isPanning;


    private void Update()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        if (Input.touchCount > 0)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                // Get movement of the finger since last frame
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

                // Move object across XY plane
                transform.Translate(-touchDeltaPosition.x * speedSwipeCamera, 0, 0);
                Vector3 pos = transform.position;
                pos.x = Mathf.Clamp(pos.x, leftLimit, rightLimit);
                transform.position = pos;
            }
        }
#else
        if (Input.GetMouseButtonDown(1))
        {
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isPanning = true;
        }

        if (!Input.GetMouseButton(1)) isPanning = false;

        if (isPanning)
        {
            Vector3 posMouse = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            Vector3 move = new Vector3(posMouse.x * speedSwipeCameraMouse, 0, 0);
            transform.Translate(move, Space.Self);
            Vector3 pos = transform.position;
            pos.x = Mathf.Clamp(pos.x, leftLimit, rightLimit);
            transform.position = pos;
        }
#endif
    }




}
